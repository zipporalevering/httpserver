package nl.hanze.web.t41.http;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class HTTPHandlerImpl implements HTTPHandler {

	public void handleRequest(InputStream in, OutputStream out) {
		/*
		 ***  OPGAVE 4: 1c ***
		 stel de juiste bestand-typen in.
		*/
		setDataTypes();
		
		HTTPRequest request = new HTTPRequest(in);
		HTTPRespons respons = new HTTPRespons(out);	
		
		request.setUri();						
		respons.setRequest(request);
		
		showDateAndTime();
		System.out.println(": " + request.getUri());
		
		try {
			respons.sendResponse();			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void showDateAndTime () {
		DateFormat format = new SimpleDateFormat("dd-mm-yyyy HH:mm:ss");
		Date date = new Date();
		System.out.print(format.format(date));
		
	}
	
	public void setDataTypes() {
		
		HTTPSettings.dataTypes.put("html", "text/html");
		HTTPSettings.dataTypes.put("css", "text/css");
		HTTPSettings.dataTypes.put("gif","image/gif");
		HTTPSettings.dataTypes.put("png","image/png");
		HTTPSettings.dataTypes.put("jpeg","image/jpeg");
		HTTPSettings.dataTypes.put("jpg","image/jpeg");
		HTTPSettings.dataTypes.put("js","application/x-javascript");
		HTTPSettings.dataTypes.put("txt","text/plain");
		HTTPSettings.dataTypes.put("pdf","application/pdf");
		
	}
	
	
	
}
